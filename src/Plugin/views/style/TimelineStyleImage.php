<?php

namespace Drupal\timeline_styles\Plugin\views\style;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * A Views style that renders markup for Timeline Styles.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "timeline_styles_image",
 *   title = @Translation("Timeline Styles With Image"),
 *   help = @Translation("To create timeline style."),
 *   theme = "timeline_styles_image",
 *   display_types = {"normal"}
 * )
 */
class TimelineStyleImage extends StylePluginBase {

  /**
   * Does this Style plugin allow Row plugins?
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Does the Style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['timeline_style_image_field'] = ['default' => 'timeline_styles_image'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $options = $this->displayHandler->getFieldLabels(TRUE);
    $form['timeline_style_image_field'] = [
      '#title' => $this->t('Timeline Style Field'),
      '#description' => $this->t('Select the field that will be used as the Event title. The rest of the fields will show up in the content.'),
      '#type' => 'select',
      '#default_value' => $this->options['timeline_style_image_field'],
      '#options' => $options,
    ];
  }
}
