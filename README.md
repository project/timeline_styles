# TIMELINE STYLES

 * Installation
 * Configuration
 * How To Use Timeline Styles?
 * Troubleshooting
 * Faq
 * maintainers

## Installation

 * Install as you would normally install a Drupal module. Visit
   https://www.drupal.org/project/timeline_styles/ for further information.

## Configuration

 * The module has no menu or modifiable settings. There is no   configuration. When
    enabled, the module will prevent the links from appearing. To get the links
    back, disable the module and clear caches.

## How To Use Timeline Styles?

 * You can add timeline from content and user can change view for timeline styles from content type and select manage display

## Troubleshooting

  * If the timeline styles does not working, check the following:
   - Try to clear cache from admin>configuration>development
     and reaload the site.

## Faq

 Q: I enabled "Timeline Styles mosule", but configuration throwing some errors. Is this normal?

 A: Yes, try to enable and disable module from command .


## maintainers

 Current maintainers:
 * Dhruvesh Tripathi - https://www.drupal.org/u/dhruveshdtripathi
 * Mahesh Hadiya - https://www.drupal.org/u/maheshhadiya77
 * Rahul Shinde - https://www.drupal.org/u/rahulshinde

This project has been sponsored by:
 * ATLAS SOFTWEB PVT LTD
   Specialized in consulting and planning of Drupal powered sites, ATLAS SOFTWEB offers installation, development, theming, customization, and hosting
   to get you started. Visit https://www.atlassoftweb.com for more information.
   